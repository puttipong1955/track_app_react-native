import CreateDataContext from "./createDataContext";
import trackerApi from "../api/tracker";
import { AsyncStorage } from "react-native";
import { setNavigator, navigate } from "../navigationRef";

const authReducer = (state, action) => {
  switch (action.type) {
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "signin":
      return { errorMessage: "", token: action.payload };
    case "clear_error_message":
      return { ...state, errorMessage: "" };
    default:
      return state;
  }
};

const signup = (dispatch) => {
  return async ({ email, password }, callback) => {
    try {
      const response = await trackerApi.post("/signup", { email, password });
      //Store token in device
      await AsyncStorage.setItem("token", response.data.token);
      dispatch({
        type: "signin",
        payload: response.data.token,
      });

      // Navigate to mainflow
      navigate("TrackList");
    } catch (error) {
      dispatch({
        type: "add_error",
        payload: "Someting went wrong with sign up",
      });
    }
  };
};

const signin = (dispatch) => {
  return async ({ email, password }) => {
    try {
      const response = await trackerApi.post("/signin", { email, password });
      //Store token in device
      await AsyncStorage.setItem("token", response.data.token);

      dispatch({
        type: "signin",
        payload: response.data.token,
      });

      // Navigate to mainflow
      navigate("TrackList");
    } catch (error) {
      dispatch({
        type: "add_error",
        payload: "Someting went wrong with sign in",
      });
    }
  };
};

const logout = (dispatch) => {
  return () => {
    // somehow sign out!
  };
};

const clearErrorMessage = (dispatch) => () => {
  dispatch({ type: "clear_error_message" });
};

export const { Provider, Context } = CreateDataContext(
  authReducer,
  { signup, signin, logout, clearErrorMessage },
  { token: null, errors: "" }
);
